import {
    Button,
    Card,
    Form,
    FormLayout,
    Layout,
    Page,
    Stack,
    TextField,
    SettingToggle,
    TextStyle
} from '@shopify/polaris';

class Mammad extends React.Component {
    state = {
        ez: true
    }

    render() {
        const ez = this.state.ez;
        let text;
        if (ez){
            text = "mammad ez ast";
        }else{
            text = "mammad ez nist";
        }
        return (
            <Page>
                <Layout>
                    {text}
                </Layout>
            </Page>
        );
    }
}

export default Mammad;