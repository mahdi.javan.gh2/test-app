import { Page, Layout, EmptyState } from '@shopify/polaris';
import {TitleBar, ResourcePicker} from '@shopify/app-bridge-react'
const img = 'https://pinkorblue.info/plugin-statics/RooBeRah_Logo-04.png';

class Index extends React.Component {
    state = {open:false}
    render(){
        return (
            <Page>
                <TitleBar
                    title='Test App'
                    primaryAction={{
                        content: 'Select Products',
                    }}
                />
                <ResourcePicker
                    open={this.state.open}
                    resourceType="Product"
                    showVariants={false}
                    onCancel={() => this.setState({open: false})}
                    onSelection={(resources) => this.handleSelection(resources)}
                />
                <Layout>
                    <EmptyState
                        heading="Discount your products temporarily"
                        action={{
                            content: 'Select products',
                        onAction: () => this.setState({open:true}),
                        }}
                        image={img}
                    >
                        <p>Select products to change their price temporarily.</p>
                    </EmptyState>
                </Layout>
            </Page>
        );
    }

    handleSelection = (resources) => {
        const ids = resources.selection.map((p) => p.id);
        this.setState({open: false});
        console.log(ids);
    };
}

export default Index;
